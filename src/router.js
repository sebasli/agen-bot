import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Chat/Home.vue";
import Control from "./views/Control/Control.vue";
import User from "./views/User/User.vue";
import UserUpdate from "./views/User/UserUpdate.vue";
import Supervisor from "./views/User/Supervisor.vue";
import SupervisorUpdate from "./views/User/SupervisorUpdate.vue";
import Supervisores from "./views/User/Supervisores.vue";
import Settings from "./views/Setting/Settings.vue";
import SettingsCap from "./views/Setting/SettingsCap.vue";
import QRFile from "./views/Setting/QRFile.vue";
import Tipificaciones from "./views/Setting/Tipificaciones.vue";
import Shorcut from "./views/Setting/Shorcut.vue";
import Tmo from "./views/Setting/Tmo.vue";

import Login from "./components/User/Login.vue";
import axios from "axios";

var pathServer = process.env.VUE_APP_URL_SERVER_EXP;

var localPathData = process.env.VUE_APP_LOCAL_DATA;
var localPathJToken = process.env.VUE_APP_LOCAL_TOKEN;
Vue.use(Router);

const router = new Router({
  base: process.env.BASE_URL,
  routes: [
    { 
      path: '*',
      meta: { requiresAuth: true }
    },
    {
      path: "/",
      component: Home,
      meta: { requiresAuth: true },
      name: "home"
    },
    {
      path: "/Login",
      component: Login,
      meta: { Logi: true },
      name: "login"
    },
    {
      path: "/control",
      component: Control,
      meta: { requiresAuthSup: true },
      name: "control"
    },
    {
      path: "/user",
      component: User,
      meta: { requiresAuthSup: true },
      name: "user"
    },
    {
      path: "/user/update",
      component: UserUpdate,
      meta: { requiresAuthSup: true },
      name: "userUpdate"
    },
    {
      path: "/supervisor",
      component: Supervisor,
      meta: { requiresAuthAdministrador: true },
      name: "supervisor"
    },
    {
      path: "/supervisor/update/",
      component: SupervisorUpdate,
      meta: { requiresAuthAdministrador: true },
      name: "supervisorUpdate"
    },
    {
      path: "/supervisores",
      component: Supervisores,
      meta: { requiresAuthAdministrador: true },
      name: "supervisores"
    },
    {
      path: "/settings",
      component: Settings,
      meta: { requiresAuthSup: true },
      name: "settings"
    },
    {
      path: "/settings/capacity",
      component: SettingsCap,
      meta: { requiresAuthSup: true },
      name: "settingsCap"
    },
    {
      path: "/settings/qr",
      component: QRFile,
      meta: { requiresAuthSup: true },
      name: "qrfile"
    },
    {
      path: "/settings/tmo",
      component: Tmo,
      meta: { requiresAuthSup: true },
      name: "tmo"
    },
    {
      path: "/settings/tipificaciones",
      component: Tipificaciones,
      meta: { requiresAuthSup: true },
      name: "tipfi"
    },
    {
      path: "/settings/atajos",
      component: Shorcut,
      meta: { requiresAuthSup: true },
      name: "short"
    }
  ]
});
router.beforeEach((to, from, next) => {
  const authUser = localStorage.getItem(localPathJToken);
  const dataBot = JSON.parse(localStorage.getItem(localPathData));
  if (to.meta.requiresAuth) {
    if (authUser && dataBot) {
      axios
        .post(
          pathServer + "/checkAgent",
          {
            idAgent: window.idAg,
            tokenUser: localStorage.getItem(localPathJToken)
          },
          {
            headers: {
              "Content-Type": "application/json",
              authorization:
                "Bearer " +
                JSON.parse(localStorage.getItem(localPathData)).jsonWebToken
            }
          }
        )
        .then(resp => {
          if (resp.data === "success") {
            next();
          } else {
            next({ name: "control" });
          }
        });
    } else {
      next({ name: "login" });
    }
  } else if (to.meta.Logi) {
    if (!dataBot) {
      next();
    } else if (dataBot) {
      next({ name: "home" });
    }
  } else if (to.meta.requiresAuthSup) {
    if (authUser) {
      axios
        .post(
          pathServer + "/checkSup",
          {
            idAgent: window.idAg,
            tokenUser: localStorage.getItem(localPathJToken)
          },
          {
            headers: {
              "Content-Type": "application/json",
              authorization:
                "Bearer " +
                JSON.parse(localStorage.getItem(localPathData)).jsonWebToken
            }
          }
        )
        .then(resp => {
          if (resp.data === "success") {
            next();
          } else {
            next({ name: "home" });
          }
        });
    } else {
      next({ name: "login" });
    }
  } else if (to.meta.requiresAuthAdministrador) {
    if (authUser) {
      axios
        .post(
          pathServer + "/checkAdministrador",
          {
            idAgent: window.idAg,
            tokenUser: localStorage.getItem(localPathJToken)
          },
          {
            headers: {
              "Content-Type": "application/json",
              authorization:
                "Bearer " +
                JSON.parse(localStorage.getItem(localPathData)).jsonWebToken
            }
          }
        )
        .then(resp => {
          if (resp.data === "success") {
            next();
          } else {
            next({ name: "home" });
          }
        });
    } else {
      next({ name: "login" });
    }
  }
});

export default router;
